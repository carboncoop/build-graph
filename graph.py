import json
from dateutil.parser import parse as parse_dt
import matplotlib.pyplot as plt

with open("filtered.json", "r") as infile:
    db = json.load(infile)

for stage in db:
    if len(db[stage]) < 10:
        continue

    x = [parse_dt(row["time"]) for row in db[stage]]
    y = [row["duration"] for row in db[stage]]

    fig = plt.figure()
    plt.plot(x, y)
    plt.gcf().autofmt_xdate()
    fig.suptitle(f"{stage}")
    fig.savefig(f"graph_{stage}.png", bbox_inches="tight")
