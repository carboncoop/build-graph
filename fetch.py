import requests
import json

API_KEY = "YOUR API KEY HERE"
PROJECT_ID = 9877735


def get_gitlab_page(project: int, page: int):
    url = f"https://gitlab.com/api/v4/projects/{project}/jobs?per_page=100&page={page}"
    r = requests.get(
        url,
        headers={
            "Authorization": f"Bearer {API_KEY}",
        },
    )
    r.raise_for_status()
    return r


try:
    with open("db.json", "r") as infile:
        db = json.load(infile)
except FileNotFoundError:
    db = dict()


def add_to_db(jobs: list):
    total = 0
    for job in jobs:
        job_id = str(job["id"])
        if job_id not in db:
            db[job_id] = job
            total += 1
    return total

print(f"Fetching page 1...")
eins = get_gitlab_page(PROJECT_ID, 1)
add_to_db(eins.json())

total = int(eins.headers["x-total-pages"])
for page in range(2, total + 1):
    print(f"Fetching page {page}...")
    r = get_gitlab_page(PROJECT_ID, page)
    if add_to_db(r.json()) == 0:
        # We don't need to go any further, we have all the data to this point
        break


with open("db.json", "w") as outfile:
    json.dump(db, outfile, indent=4)
