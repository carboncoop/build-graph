import json

with open("db.json", "r") as infile:
    db = json.load(infile)

KEYS = {"created_at", "duration", "name"}


def include_keys(dictionary, keys):
    """Filters a dict by only including certain keys."""
    key_set = set(keys) & set(dictionary.keys())
    return {key: dictionary[key] for key in key_set}


filtered = [
    include_keys(row, KEYS) for row in db.values() if row["status"] == "success"
]

names = {row["name"] for row in filtered}

final = dict()

for name in names:
    final[name] = [
        {"time": row["created_at"], "duration": row["duration"]}
        for row in filtered
        if row["name"] == name
    ]

with open("filtered.json", "w") as outfile:
    json.dump(final, outfile)
